import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()

let baseUrl="http://192.168.0.4:8282/renren-fast"
Vue.prototype.url={
	"wx":{
			"login":baseUrl+"/app/wx/login",
			"microAppPayOrder":baseUrl+"/app/wx/microAppPayOrder",
			"updateOrderStatus":baseUrl+"/app/wx/updateOrderStatus"
	},
	"zfb":{
		"login":baseUrl+"/app/zfb/login",
		"microAppPayOrder":baseUrl+"/app/zfb/microAppPayOrder",
		"updateOrderStatus":baseUrl+"/app/zfb/updateOrderStatus",
		"appPayOrder": baseUrl + "/app/zfb/appPayOrder"
	},
	"login":baseUrl+"/app/user/login",
	"searchUserOrderList":baseUrl+"/app/order/searchUserOrderList",

}

Vue.prototype.zfb={
	app:{
		appid:"2016110100783462",
		privateKey:"MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCJX+2n5Ck5/fsSnw0hEkMzrT7Kut9pI66G3xtQzdNPCvp5046yz7XYEZnYSbs98Yrx/7XMVQ7AkZWgupuQKooU35V0sdl/oW6/KSYcMX7fHvQ/eh0fGWA2ZRReI2yCL+miHygs4iYPb0+Mgxn48CuU96fMzKm0KnTEk//KIsH+2m8omftaMIPYnlQMviUXJ/0FpkOwuphaNPcO7+cmuU7M2qpQ/W/gd9Xk/TX9wcjfeRVqhe4N1kL0gbzYjtC+vpm/wAPgc+Z8bHAG/bJkZDXfTuWcRJ3GQoB3brV5eTSQrOBJOr7F2B3rSmaEbje1hjI4pQlbwtYR1ORgu+V2eWEnAgMBAAECggEADXDBQ8fi8LWUtwGwuyeaQ5gTqKoX6aWAEdoKRiGdJwKf9wNn1r7Tv8wZDTxJgYn8Wij2lINN6jFUPmGDjttanygw6zXrXb1hpHpWLDP7gcD5jKxZ5Q0KiMfi8kLs/t/JL/90Ys/NgX0Wf1zkUaLN4MlKNZNglywAOKtyP0ifSlvX9GxGxevj9ke/tpGoiHEPgQMzOa0UEusuFok/eFZ3N5P/rkYEa/nXGsbrnvOQwfGHceVOH5vDlhZGDdLs2KQBd0LNoZvOrMNvpKcjC4WxWTuKURKtiXxGP8lXa8BrCyHGQyv7fi2xy4Y7V97edKx08OAxrapof3fhV+4+5jgl4QKBgQDIvY1oG9KFmSo0V806xn+tzsTCywaqxXzid31bmQW4lba0Jv7RO2CRKj6/pAJ1aGbCD8gy1PB1V6dMevR2EraL/LHue7omtedZjLPvW7qrjPEO+RVRuA+zO3tivYKajkRb3BMALaqjJF5vtxXTzelvVAnpXJi1e2DPHZzAXmZ4GwKBgQCvMOistTzcJR1TrTh/O1qJlP8J4qgKU8AvWk8C8jxO6EeauX9j9IgNxJCIUm8CHgbucljMwLo8sL3Yzk/8oC36d6plEU3wnv1TetDhbjPOZaejWV0G5gqRWBL5vMSp1nU8177f4rXdbF7VgB8f4/uPGMhgwstWprpRovVZnfDj5QKBgBvhJ5iD9LMVWKU4ZIp7MxRimTUsmVpyXyvcyI5GLGM8d8mBePZ6NqkyotBe6PCBdXQSWQ2mQVcqu1OF6BnV84BxsgybJnll3iGzxvHBXaWhx4D62xkkZRnRcwt9onvwslHHuOPXc9GApZUCaoemG3uYwl+kupjJ7+IUDMcy1SxtAoGAIf1ANznWe/Orc++YlX7DewvwS0+xQIetMsK3r5+lub1xHwC8SX1I5Drtu7UamnOO7rY6qkb+GwZDXwvwEthU81JCOeeYkoWWKpsaaZJC9NleI+ATn2IETNykmdVY5oaYKlzFSJSoqjP7VjrMTxihVsr9HWDNqJsibPJL3b453U0CgYBOs0NCnjRJccS0vg6KEvmPZICJ2CJz2l8t/vZQKDGmgBOapNHkNyA3UKqQg43GWnzDUtx5vXHh8MZsepw/Mbw7GK5AfxkZ97DOFoKLsiFFzLPKMggRUAGt696RJ6ht/P2hKq70TAsXqNfD6WS9pQyKFKkauBiRy2Wv4T8Y6FnE2g=="
	}
}

//Vue.prototype.appEnv="H5APP"
Vue.prototype.appEnv="微信小程序"
//Vue.prototype.appEnv="支付宝小程序"